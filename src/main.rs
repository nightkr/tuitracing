use futures::{channel::mpsc, future, pin_mut, Stream, StreamExt};
use rand::Rng;
use rand_distr::Normal;
use std::{
    collections::VecDeque,
    io::{stdin, stdout, Write},
    time::Duration,
};
use termion::{
    event::{Event, Key},
    input::TermRead,
    raw::IntoRawMode,
};
use tokio::task::spawn_blocking;
use tokio_stream::wrappers::IntervalStream;
use tracing_subscriber::fmt::MakeWriter;
use tui::{
    backend::TermionBackend,
    layout::{Constraint, Direction, Layout},
    text::{Spans, Text},
    widgets::{BarChart, Block, Borders, Paragraph},
    Terminal,
};

#[tokio::main]
async fn main() {
    let (cmd_tx, cmd_rx) = mpsc::unbounded::<RenderCmd>();
    tracing_subscriber::fmt()
        .with_writer(LogWriter { tx: cmd_tx.clone() })
        .with_ansi(false)
        .pretty()
        .init();

    let mut term = Terminal::new(TermionBackend::new(stdout().into_raw_mode().unwrap())).unwrap();
    let quit_event = spawn_blocking(|| {
        stdin()
            .events()
            .take_while(|ev| !matches!(ev.as_ref().unwrap(), Event::Key(Key::Ctrl('c'))))
            .for_each(|_| ())
    });

    let _ = future::join(
        volume_detector()
            .map(RenderCmd::UpdateVolume)
            .map(Ok)
            .forward(cmd_tx),
        renderer(&mut term, cmd_rx.take_until(quit_event)),
    )
    .await;
    term.clear().unwrap();
    term.draw(|_| {}).unwrap();
}

#[derive(Clone)]
struct LogWriter {
    tx: mpsc::UnboundedSender<RenderCmd>,
}

impl Write for LogWriter {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        let _ = self.tx.unbounded_send(RenderCmd::LogLine(
            std::str::from_utf8(buf).unwrap().to_string(),
        ));
        Ok(buf.len())
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}

impl MakeWriter for LogWriter {
    type Writer = Self;

    fn make_writer(&self) -> Self::Writer {
        self.clone()
    }
}

fn volume_detector() -> impl Stream<Item = Volumes> {
    fn move_volume(prev: u8) -> u8 {
        let add = rand::random();
        let magnitude = rand::thread_rng()
            .sample(Normal::<f32>::new(4.0, 8.0).unwrap())
            .clamp(0.0, 255.0)
            .round() as u8;
        if add {
            prev.saturating_add(magnitude)
        } else {
            prev.saturating_sub(magnitude)
        }
    }

    IntervalStream::new(tokio::time::interval(Duration::from_millis(100))).scan(
        Volumes::default(),
        |volumes, _| {
            tracing::info!("Reading volumes");
            *volumes = Volumes {
                headset: move_volume(volumes.headset),
                speaker: move_volume(volumes.speaker),
                mic: move_volume(volumes.mic),
            };
            let curr = *volumes;
            async move { Some(curr) }
        },
    )
}

#[derive(Default, Clone, Copy)]
struct Volumes {
    headset: u8,
    speaker: u8,
    mic: u8,
}

enum RenderCmd {
    UpdateVolume(Volumes),
    LogLine(String),
}

async fn renderer(
    term: &mut Terminal<impl tui::backend::Backend>,
    cmds: impl Stream<Item = RenderCmd>,
) {
    pin_mut!(cmds);
    term.clear().unwrap();

    let mut volumes = Volumes::default();
    let mut log_lines = VecDeque::<String>::new();

    while let Some(cmd) = cmds.next().await {
        match cmd {
            RenderCmd::UpdateVolume(vols) => volumes = vols,
            RenderCmd::LogLine(line) => log_lines.push_back(line),
        }

        term.draw(|f| {
            let chunks = Layout::default()
                .direction(Direction::Horizontal)
                .margin(1)
                .constraints([Constraint::Percentage(50), Constraint::Percentage(50)])
                .split(f.size());
            let log_block = Block::default().title("Logs").borders(Borders::ALL);
            while log_lines.len() > log_block.inner(chunks[0]).height as usize {
                log_lines.pop_front();
            }
            let mut log_text = Text::default();
            log_text.extend(log_lines.iter().map(String::as_str).map(Spans::from));
            f.render_widget(Paragraph::new(log_text).block(log_block), chunks[0]);
            f.render_widget(
                BarChart::default()
                    .block(Block::default().title("Volume").borders(Borders::ALL))
                    .bar_width(10)
                    .data(&[
                        ("Headset", volumes.headset as u64),
                        ("Speaker", volumes.speaker as u64),
                        ("Mic", volumes.mic as u64),
                    ])
                    .max(255),
                chunks[1],
            )
        })
        .unwrap();
    }
}
